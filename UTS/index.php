<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>UTS-Aura</title>
<link rel="stylesheet" href="./style.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

<script>
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	var actions = $("table td:last-child").html();
    
    $(".add-new").click(function(){
		$(this).attr("disabled", "disabled");
		var index = $("table tbody tr:last-child").index();
        var row = '<tr>' +
            '<td><input type="text" class="form-control" name="id" id="id"></td>' +
            '<td><input type="text" class="form-control" name="name" id="name"></td>' +
            '<td><input type="text" class="form-control" name="gender" id="gender"></td>' +
            '<td><input type="text" class="form-control" name="age" id="age"></td>' +
            '<td><input type="text" class="form-control" name="1" id="1"></td>' +
			'<td><input type="text" class="form-control" name="2" id="2"></td>' +
			'<td><input type="text" class="form-control" name="action" id="action"></td>' +
			'<td>' + actions + '</td>' +
        '</tr>';
    	$("table").append(row);		
		$("table tbody tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();
    });
	
    $(document).on("click", ".add", function(){
		var empty = false;
		var input = $(this).parents("tr").find('input[type="text"]');
        input.each(function(){
			if(!$(this).val()){
				$(this).addClass("error");
				empty = true;
			} else{
                $(this).removeClass("error");
            }
		});
		$(this).parents("tr").find(".error").first().focus();
		if(!empty){
			input.each(function(){
				$(this).parent("td").html($(this).val());
			});			
			$(this).parents("tr").find(".add, .edit").toggle();
			$(".add-new").removeAttr("disabled");
		}		
    });
    
	$(document).on("click", ".edit", function(){		
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
			$(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
		});		
		$(this).parents("tr").find(".add, .edit").toggle();
		$(".add-new").attr("disabled", "disabled");
    });
	
    $(document).on("click", ".delete", function(){
        $(this).parents("tr").remove();
		$(".add-new").removeAttr("disabled");
    });
});
</script>
</head>
<body>
<div class="container">
        <div class="row">
            <div class="col-lg-12">
                <header class="text-center tm-site-header">
                    <div class="tm-site-logo"></div>
                    <h1 class="pl-4 tm-site-title">Girl Group Korea</h1>
                </header>
            </div>
        </div>
        <div class="container tm-container-2">
            <div class="row">
                <div class="col-sm-12">
					<button type="button" class="btn btn-info add-new"><i class="fa fa-plus"></i> Tambah</button>
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
						<th>Nama Grup</th>
                        <th>Deskripsi</th>
                        <th>Asal</th>
                        <th>Genre</th>
                        <th>Label Management</th>
						<th>Anggota</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1.</td>
                        <td>Aespa 에스파</td>
                        <td> aespa (에스파) adalah girl grup asal Korea Selatan yang dibentuk oleh SM Entertainment. <br> Grup ini terdiri dari empat anggota: Karina, Giselle, Winter dan Ningning. Mereka debut pada 17 November 2020 dengan single "Black Mamba".  </td>
						<td>Seoul, Korea Selatan</td>
						<td>K-pop, Hip-hop. EDM</td>
						<td>SM Entertainment</td>
						<td>Karina <br> Giselle <br> Winter<br> Ningning</td>
                    <tr>
                        <td>2.</td>
                        <td>BLACKPINK 블랙핑크 </td>
                        <td>Blackpink (Hangul: 블랙핑크; RR: Beullaegpingkeu) ditulis bergaya sebagai BLACKPINK atau BLΛƆKPIИK adalah grup vokal wanita asal Korea Selatan yang dibentuk oleh YG Entertainment <br>Grup ini debut pada tanggal 8 Agustus 2016, dengan album single pertama mereka berjudul Square One, dengan lagu utamanya "Whistle", lagu nomor satu pertama mereka di Korea Selatan. Single ini juga menghasilkan "Boombayah", single nomor satu pertama mereka di tangga lagu Billboard World Digital Songs, yang mendapat rekor sebagai video musik debut yang paling banyak ditonton oleh artis Korea.   </td>
						<td>Seoul, Korea Selatan</td>
						<td>K-pop, Hip-hop. EDM</td>
						<td>YG Entertainment</td>
						<td>Jisoo<br>Jennie <br> Rosé<br> Lisa</td>
                        <td>
                            <a class="add" title="Add" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a>
                            <a class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
                            <a class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
                        </td>
                    </tr>
                    </tr>  
                </tbody>
            </table>
        </div>
    </div>
</body>
    <footer>
    <?php echo "Aura Azzahra - 11118230"; ?>
    </footer>
</html>